package se.strivor.sapider.repo;

import org.springframework.data.repository.CrudRepository;
import se.strivor.sapider.data.SapMetaData;
import se.strivor.sapider.data.SapMetaDataId;

public interface SapMetaDataRepository extends CrudRepository<SapMetaData, SapMetaDataId> {

}
