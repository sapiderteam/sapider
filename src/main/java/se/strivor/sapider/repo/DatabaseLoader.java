package se.strivor.sapider.repo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import se.strivor.sapider.config.SapFunctions;
import se.strivor.sapider.data.SapFunction;
import se.strivor.sapider.data.SapMetaData;
import se.strivor.sapider.sap.SapFunctionName;

import java.util.List;
import java.util.Map;

import static se.strivor.sapider.sap.TestClient.sapGetListMetadata;

@Component
public class DatabaseLoader implements CommandLineRunner {

    private final SapFunctionRepository sapFunctionRepository;
    private final SapMetaDataRepository sapMetaDataRepository;

    @Autowired
    public DatabaseLoader(SapFunctionRepository sapFunctionRepository, SapMetaDataRepository sapMetaDataRepository) {
        this.sapFunctionRepository = sapFunctionRepository;
        this.sapMetaDataRepository = sapMetaDataRepository;
    }

    @Autowired
    private SapFunctions sapFunctions;

    @Override
    public void run(String... strings) throws Exception {
        System.out.println("===== Database Loader");
        for (SapFunctionName functionName : SapFunctionName.values()) {
            System.out.println("================================================================");
            System.out.println("===== Saving SAP Function: " + functionName.toString());
            try {
                this.sapFunctionRepository.save(new SapFunction(functionName.toString(), functionName.getDescription()));
                System.out.println("================================================================");
                // TODO: Fix for yml property file
//        for (SapFunction sapFunction : sapFunctions.getSapFunctionList()) {
//            this.sapFunctionRepository.save(sapFunction);
//            String functionName = sapFunction.getName();
                Map<String, List<SapMetaData>> sapMetaDataMap = sapGetListMetadata(functionName.toString());
                for (Map.Entry<String, List<SapMetaData>> entry : sapMetaDataMap.entrySet()) {
                    List<SapMetaData> sapMetaDataList = entry.getValue();
                    for (SapMetaData sapMetaData : sapMetaDataList) {
                        System.out.println("===== Saving SAP MetaData");
                        this.sapMetaDataRepository.save(sapMetaData);
                        System.out.println("================================================================");
                    }
                }
            }
            catch (Exception e) {
                System.out.println("################################################################");
                System.out.println("##### EXCEPTION");
                System.out.println(e.toString());
                System.out.println("################################################################");
            }

        }
    }
}
