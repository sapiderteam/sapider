package se.strivor.sapider.repo;

import org.springframework.data.repository.CrudRepository;
import se.strivor.sapider.data.SapFunction;

public interface SapFunctionRepository extends CrudRepository<SapFunction, String> {

}
