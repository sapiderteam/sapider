package se.strivor.sapider.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import se.strivor.sapider.data.SapMetaData;
import se.strivor.sapider.repo.SapMetaDataRepository;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

@Controller
public class SapMetaDataController {

    @Autowired
    SapMetaDataRepository sapMetaDataRepository;

    @RequestMapping("/sapMetaDatas")
    public String distilleries(Model model) {

        Iterable<SapMetaData> sapMetaDatas = sapMetaDataRepository.findAll();
        List<Field> fieldsList = new ArrayList();
        Field[] fields = SapMetaData.class.getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            fieldsList.add(field);
        }
        model.addAttribute("title", "SAP MetaDatas");
        model.addAttribute("entities", sapMetaDatas);
        model.addAttribute("fieldsList", fieldsList);
        return "sapListEntities";
    }
}
