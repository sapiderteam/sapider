package se.strivor.sapider.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import se.strivor.sapider.data.SapFunction;
import se.strivor.sapider.repo.SapFunctionRepository;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

@Controller
public class SapFunctionsController {

    @Autowired
    SapFunctionRepository sapFunctionRepository;

    @RequestMapping("/sapFunctions")
    public String distilleries(Model model) {

        Iterable<SapFunction> sapFunctions = sapFunctionRepository.findAll();
        List<Field> fieldsList = new ArrayList();
        Field[] fields = SapFunction.class.getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            fieldsList.add(field);
        }
        model.addAttribute("title", "SAP Functions");
        model.addAttribute("entities", sapFunctions);
        model.addAttribute("fieldsList", fieldsList);
        return "sapListEntities";
    }
}
