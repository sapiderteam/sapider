package se.strivor.sapider.data;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public @Data class SapFunction {

    public  @Id String name;
    public String description;

    public SapFunction() {}

    public SapFunction(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
