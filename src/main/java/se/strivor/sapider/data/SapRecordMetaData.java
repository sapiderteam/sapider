package se.strivor.sapider.data;

import com.sap.conn.jco.JCoRecordMetaData;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;

@Entity
@EqualsAndHashCode(callSuper = true)
public @Data class SapRecordMetaData extends SapMetaData {

    int byteOffset;
    int unicodeByteOffset;

    private SapRecordMetaData() {}

    public SapRecordMetaData(String functionName, String metaDataType, JCoRecordMetaData recordMetaData, int index) {
        super(functionName, metaDataType, recordMetaData, index);
        setByteOffset(recordMetaData.getByteOffset(index));
        setUnicodeByteOffset(recordMetaData.getUnicodeByteOffset(index));
    }

    public SapRecordMetaData(JCoRecordMetaData recordMetaData, int index) {
        super(recordMetaData, index);
        setByteOffset(recordMetaData.getByteOffset(index));
        setUnicodeByteOffset(recordMetaData.getUnicodeByteOffset(index));
    }
}
