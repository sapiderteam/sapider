package se.strivor.sapider.data;

import java.io.Serializable;

public class SapMetaDataId implements Serializable {
    String functionName;
    String metaDataType;
    String name;
}
