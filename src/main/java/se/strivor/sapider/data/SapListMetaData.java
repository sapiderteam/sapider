package se.strivor.sapider.data;

import com.sap.conn.jco.JCoListMetaData;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;

@Entity
@EqualsAndHashCode(callSuper = true)
public @Data class SapListMetaData extends SapMetaData {

    String defaultValue;
    String recordFieldName;
    boolean isChanging;
    boolean isException;
    boolean isExport;
    boolean isImport;
    boolean isOptional;

    private SapListMetaData() {}

    public SapListMetaData(String functionName, String metaDataType, JCoListMetaData listMetaData, int index) {
        super(functionName, metaDataType, listMetaData, index);
        setDefaultValue(listMetaData.getDefault(index));
        setRecordFieldName(listMetaData.getRecordFieldName(index));
        setChanging(listMetaData.isChanging(index));
        setException(listMetaData.isException(index));
        setExport(listMetaData.isExport(index));
        setImport(listMetaData.isImport(index));
        setOptional(listMetaData.isOptional(index));
    }

    public SapListMetaData(JCoListMetaData listMetaData, int index) {
        super(listMetaData, index);
        setDefaultValue(listMetaData.getDefault(index));
        setRecordFieldName(listMetaData.getRecordFieldName(index));
        setChanging(listMetaData.isChanging(index));
        setException(listMetaData.isException(index));
        setExport(listMetaData.isExport(index));
        setImport(listMetaData.isImport(index));
        setOptional(listMetaData.isOptional(index));
    }
}
