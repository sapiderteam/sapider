package se.strivor.sapider.data;

import com.sap.conn.jco.JCoExtendedFieldMetaData;
import com.sap.conn.jco.JCoMetaData;
import com.sap.conn.jco.JCoRecordMetaData;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity @IdClass(SapMetaDataId.class)
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Data public abstract class SapMetaData implements Serializable {

    @Id public String functionName;
    @Id public String metaDataType;
    @Id public String name;
    public int byteLength;
    public String classNameOfField;
    public int decimals;
    public String description;
//    public JCoExtendedFieldMetaData extendedFieldMetaData;
    public int length;
//    public JCoRecordMetaData recordMetaData;
//    public List<SapMetaData> recordMetaDataList;
    public String recordTypeName;
    public int type;
    public String typeAsString;
    public int unicodeByteLength;
    public boolean isAbapObject;
    public boolean isNestedType1Structure;
    public boolean isStructure;
    public boolean isTable;

    public SapMetaData() {}

    public SapMetaData(String functionName, String metaDataType, JCoMetaData metaData, int index) {
        setFunctionName(functionName);
        setMetaDataType(metaDataType);
        setByteLength(metaData.getByteLength(index));
        setClassNameOfField(metaData.getClassNameOfField(index));
        setDecimals(metaData.getDecimals(index));
        setDescription(metaData.getDescription(index));
//        setExtendedFieldMetaData(metaData.getExtendedFieldMetaData(index));
        setLength(metaData.getLength(index));
        setName(metaData.getName(index));
//        setRecordMetaData(metaData.getRecordMetaData(index));
        setRecordTypeName(metaData.getRecordTypeName(index));
        setType(metaData.getType(index));
        setTypeAsString(metaData.getTypeAsString(index));
        setUnicodeByteLength(metaData.getUnicodeByteLength(index));
        setAbapObject(metaData.isAbapObject(index));
        setNestedType1Structure(metaData.isNestedType1Structure(index));
        setStructure(metaData.isStructure(index));
        setTable(metaData.isTable(index));
    }

    public SapMetaData(JCoMetaData metaData, int index) {
        setByteLength(metaData.getByteLength(index));
        setClassNameOfField(metaData.getClassNameOfField(index));
        setDecimals(metaData.getDecimals(index));
        setDescription(metaData.getDescription(index));
//        setExtendedFieldMetaData(metaData.getExtendedFieldMetaData(index));
        setLength(metaData.getLength(index));
        setName(metaData.getName(index));
//        setRecordMetaData(metaData.getRecordMetaData(index));
        setRecordTypeName(metaData.getRecordTypeName(index));
        setType(metaData.getType(index));
        setTypeAsString(metaData.getTypeAsString(index));
        setUnicodeByteLength(metaData.getUnicodeByteLength(index));
        setAbapObject(metaData.isAbapObject(index));
        setNestedType1Structure(metaData.isNestedType1Structure(index));
        setStructure(metaData.isStructure(index));
        setTable(metaData.isTable(index));
    }
}
