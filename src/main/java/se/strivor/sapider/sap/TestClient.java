package se.strivor.sapider.sap;

import java.io.File;
import java.io.FileOutputStream;
import java.util.*;

import com.sap.conn.jco.*;
import com.sap.conn.jco.ext.DestinationDataProvider;
import se.strivor.sapider.data.SapListMetaData;
import se.strivor.sapider.data.SapMetaData;
import se.strivor.sapider.data.SapRecordMetaData;

import static com.sap.conn.jco.JCoDestinationManager.getDestination;

public class TestClient {
    static String DESTINATION_NAME1 = "ABAP_AS_WITHOUT_POOL";
    static String DESTINATION_NAME2 = "ABAP_AS_WITH_POOL";
    static String DESTINATION_NAME3 = "UKN";
    static String NL = "\r\n";
    static String TAB = "\t";
    
    static {
        Properties connectProperties = new Properties();
        connectProperties.setProperty(DestinationDataProvider.JCO_ASHOST, "cit0327.edc.linde.grp");
        connectProperties.setProperty(DestinationDataProvider.JCO_SYSNR, "27");
        connectProperties.setProperty(DestinationDataProvider.JCO_CLIENT, "110");
        connectProperties.setProperty(DestinationDataProvider.JCO_USER, "T03_EAS_01");
        connectProperties.setProperty(DestinationDataProvider.JCO_PASSWD, "TRE@$UrE#321");

//        connectProperties.setProperty(DestinationDataProvider.JCO_ASHOST, "10.139.113.60");
//        connectProperties.setProperty(DestinationDataProvider.JCO_SYSNR, "60");
//        connectProperties.setProperty(DestinationDataProvider.JCO_CLIENT, "100");
//        connectProperties.setProperty(DestinationDataProvider.JCO_USER, "acmrfc");
//        connectProperties.setProperty(DestinationDataProvider.JCO_PASSWD, "secure99");
        connectProperties.setProperty(DestinationDataProvider.JCO_LANG, "en");
//        createDestinationDataFile(DESTINATION_NAME2, connectProperties);
        connectProperties.setProperty(DestinationDataProvider.JCO_POOL_CAPACITY, "10");
        connectProperties.setProperty(DestinationDataProvider.JCO_PEAK_LIMIT, "10");
//        connectProperties.setProperty(DestinationDataProvider.JCO_REPOSITORY_DEST, "ABAP_AS_WITH_POOL");
//        connectProperties.setProperty(DestinationDataProvider.JCO_REPOSITORY_PASSWD, "TRE@$UrE#321");
//        connectProperties.setProperty(DestinationDataProvider.JCO_REPOSITORY_USER, "T03_EAS_01");
        createDestinationDataFile(DESTINATION_NAME2, connectProperties);
    }

    static void createDestinationDataFile(String destinationName, Properties connectProperties) {
        File destCfg = new File(destinationName + ".jcoDestination");
        try {
            FileOutputStream fos = new FileOutputStream(destCfg, false);
            connectProperties.store(fos, "for tests only !");
            fos.close();
        } catch (Exception e) {
            throw new RuntimeException("Unable to create the destination files", e);
        }
    }

    public static JCoDestination sapGetDestination() throws JCoException {
        return sapGetDestination(DESTINATION_NAME2);
    }

    public static JCoDestination sapGetDestination(String destinationName) throws JCoException {
        return getDestination(destinationName);
    }

    public static JCoRepository sapGetRepository(JCoDestination destination) throws JCoException {
        JCoRepository repository = destination.getRepository();
        return repository;
    }

    public static JCoFunction sapGetFunction(String functionName) throws JCoException {
        JCoDestination destination = sapGetDestination();
        JCoFunction function = sapGetRepository(destination).getFunction(functionName);
        if (function == null) {
            System.out.println("================================================================");
            System.out.println(">>>>>>>>>> Function '" + functionName + "' not found in SAP. <<<<<<<<<<");
            System.out.println("================================================================");
        }
        try {
            /*
Material nr: 116547 Produkt: ACETYLEN NEMO PLUS 21 L
Material nr:  100214 Produkt: ACETYLEN 21 L
Material nr: 107216 Produkt: CHEMICAL NITROGEN 4.6 20

SAP Sold To (kund nr): 251637 Namn: OUTOKUMPU, AVESTA JERNVERK
             */
            if ("Y_NET_VALUE_KUNNR_MATNR".equals(function.getName())) {
                function.getImportParameterList().setValue("P_I_KPOSN","000000"); //   <!-- AGA Contract Item no (6) -->
                function.getImportParameterList().setValue("P_I_KUNNR", "0000346982"); //   <!-- Sold to party (customer) (10) -->
                function.getImportParameterList().setValue("P_I_KVGR5","1.00"); //    <!-- Obsolete (6) -->
                function.getImportParameterList().setValue("P_I_MATNR", "000000000000100214"); //   <!-- Material number (product) (18) -->
                function.getImportParameterList().setValue("P_I_NO_ZONE"," "); // <!-- 'X' = Use no Zone surcharge (1) -->
                function.getImportParameterList().setValue("P_I_PLANT"," "); //  <!-- Plant (4) -->
                //function.getImportParameterList().setValue("P_I_VALID_ON","2017-07-24"); // <!-- Pricing date (8) -->
                function.getImportParameterList().setValue("P_I_VBELN",""); //  <!-- AGA Contract no (10) -->
                function.getImportParameterList().setValue("P_I_VKORG","SE10"); //  <!-- Sales organization  Case 00510 (4) -->
                function.getImportParameterList().setValue("P_I_VTWEG","31"); //  <!-- Distribution channel (2) -->
                function.getImportParameterList().setValue("P_I_YYLEASE_NBR_YR","00"); // <!-- Lease the number of years sold (2) -->
                function.execute(destination);
            }
            else if ("Y_ADR_DECLARATION".equals(function.getName())) {
                function.getImportParameterList().setValue("MATNR", "000000000000116547");
                function.getImportParameterList().setValue("QUANTITY", "1.00");
                function.getImportParameterList().setValue("MEINS", "CYL");
                function.getImportParameterList().setValue("SPRAS", "EN");
                function.execute(destination);
            }
            else if ("/AGA/SDF_GET_MATERIAL".equals(function.getName())) {
                function.getImportParameterList().setValue("I_SPRAS", "EN");
                function.execute(destination);
            }
            else if ("/AGA/SDF_SEARCH_MATERIAL".equals(function.getName())) {
                function.getImportParameterList().setValue("I_WERKS", "4501");
                function.getImportParameterList().setValue("I_SPRAS", "E");
                function.getImportParameterList().setValue("I_VKORG", "SE10");
                function.getImportParameterList().setValue("I_VTWEG", "NR");
                function.execute(destination);
            }
            else if ("/AGA/SDF_GET_STOCK_INFO".equals(function.getName())) {
                function.getImportParameterList().setValue("I_MATNR", "                  ");
                function.getImportParameterList().setValue("I_PLANT", "4501");
                function.getImportParameterList().setValue("I_SELECT_TYPE", " ");
                function.getImportParameterList().setValue("I_SPRAS", "en");
                function.execute(destination);
            }
            else if ("/AGA/SDF_SEARCH_CUSTOMER".equals(function.getName())) {
                function.getImportParameterList().setValue("I_SEARCH_TYPE", "C");
                function.getImportParameterList().setValue("I_LAND1", "SE");
                function.getImportParameterList().setValue("I_VKORG", "SE10");
                //function.getImportParameterList().setValue("I_KUNNR_AG", "104");
                function.execute(destination);
            }
        } catch (AbapException e) {
            System.out.println("========================================================================================");
            System.out.println(e.toString());
            System.out.println("========================================================================================");
        }
        return function;
    }

    private static List<SapMetaData> createMetaDataList(String functionName, JCoMetaData jcoMetaData) {
        List<SapMetaData> sapMetaDataList = new ArrayList<>();
        for (int i = 0; i < jcoMetaData.getFieldCount(); i++) {
            SapMetaData sapMetaData = null;
            if (jcoMetaData instanceof JCoListMetaData) {
                sapMetaData = new SapListMetaData(functionName, jcoMetaData.getName(), (JCoListMetaData)jcoMetaData, i);
            }
            else if (jcoMetaData instanceof JCoRecordMetaData) {
                sapMetaData = new SapRecordMetaData(functionName, jcoMetaData.getName(), (JCoRecordMetaData)jcoMetaData, i);
            }
//            if (sapMetaData != null && sapMetaData.getRecordMetaData() != null) {
//                sapMetaData.setRecordMetaDataList(createMetaDataList(sapMetaData.getRecordMetaData()));
//            }
            sapMetaDataList.add(sapMetaData);
        }
        return sapMetaDataList;
    }

    public static Map<String, List<SapMetaData>> sapGetListMetadata(String functionName) throws JCoException {
        Map<String, List<SapMetaData>> metaDataMap = new HashMap<>();
        JCoFunction function = sapGetFunction(functionName);
        JCoListMetaData listMetaData;
        if (function == null) {
            return null;
        }
        if (function.getImportParameterList() != null) {
            listMetaData = function.getImportParameterList().getListMetaData();
            metaDataMap.put(listMetaData.getName(), createMetaDataList(functionName, listMetaData));
        }

        if (function.getExportParameterList() != null) {
            listMetaData = function.getExportParameterList().getListMetaData();
            metaDataMap.put(listMetaData.getName(), createMetaDataList(functionName, listMetaData));
            System.out.print("==================================================================================" + NL);
            for (JCoField field : function.getExportParameterList()) {
                System.out.print(field.getName() + ":" + TAB + field.getString() + NL);
            }
            System.out.print("==================================================================================" + NL);
        }

        if (function.getChangingParameterList() != null) {
            listMetaData = function.getChangingParameterList().getListMetaData();
            metaDataMap.put(listMetaData.getName(), createMetaDataList(functionName, listMetaData));
        }

        if (function.getTableParameterList() != null) {
            listMetaData = function.getTableParameterList().getListMetaData();
            metaDataMap.put(listMetaData.getName(), createMetaDataList(functionName, listMetaData));
        }

        if (function.getFunctionTemplate().getFunctionInterface() != null) {
            listMetaData = function.getFunctionTemplate().getFunctionInterface();
            metaDataMap.put(listMetaData.getName(), createMetaDataList(functionName, listMetaData));
        }
        return metaDataMap;
    }

    public static String sapGetFunctionDetails(JCoFunction function) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append(function.getName()).append(NL)
                .append("==================================================================================").append(NL)
                .append("-----Function Template").append(NL)
                .append(function.getFunctionTemplate().toString())
                .append("==================================================================================").append(NL)
                .append("-----Function Template - Import Parameter List").append(NL)
                .append(function.getFunctionTemplate().getImportParameterList().toString())
                .append("==================================================================================").append(NL)
                .append("-----Function Template - Export Parameter List").append(NL)
                .append(function.getFunctionTemplate().getExportParameterList().toString())
                .append("==================================================================================").append(NL)
                .append("-----Import Parameter List").append(NL)
                .append(function.getImportParameterList().toString()).append(NL)
                .append("==================================================================================").append(NL)
                .append("-----Export Parameter List").append(NL)
                .append(function.getExportParameterList().toString()).append(NL)
                .append("==================================================================================").append(NL);
//        JCoListMetaData importParameterList = function.getFunctionTemplate().getImportParameterList();
        return stringBuilder.toString();
    }

    private static String getAttributes(JCoDestination destination) throws JCoException {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Attributes:" + NL);
        stringBuilder.append(destination.getAttributes() + NL);
        System.out.println(stringBuilder.toString());
        return stringBuilder.toString();
    }

    public static String step1Connect() throws JCoException {
        JCoDestination destination = sapGetDestination(DESTINATION_NAME1);
        return getAttributes(destination);
    }

    public static String step2ConnectUsingPool() throws JCoException {
        JCoDestination destination = sapGetDestination(DESTINATION_NAME2);
        return getAttributes(destination);
    }

    public static String step3SimpleCall() throws JCoException {
        StringBuilder stringBuilder = new StringBuilder();
        JCoDestination destination = sapGetDestination(DESTINATION_NAME2);
        JCoFunction function = destination.getRepository().getFunction("STFC_CONNECTION");
        if (function == null) {
            throw new RuntimeException("BAPI_COMPANYCODE_GETLIST not found in SAP.");
        }
        function.getImportParameterList().setValue("REQUTEXT", "Hello SAP");

        try {
            function.execute(destination);
        } catch (AbapException e) {
            System.out.println(e.toString());
            return e.toString();
        }

        stringBuilder.append("STFC_CONNECTION finished:" + NL);
        stringBuilder.append(" Echo: " + function.getExportParameterList().getString("ECHOTEXT") + NL);
        stringBuilder.append(" Response: " + function.getExportParameterList().getString("RESPTEXT") + NL + NL);
        System.out.println(stringBuilder.toString());
        return stringBuilder.toString();
    }

    public static String step3WorkWithStructure() throws JCoException {
        JCoDestination destination = sapGetDestination(DESTINATION_NAME2);
        JCoFunction function = destination.getRepository().getFunction("RFC_SYSTEM_INFO");
        StringBuilder stringBuilder = new StringBuilder();
        if (function == null) {
            throw new RuntimeException("BAPI_COMPANYCODE_GETLIST not found in SAP.");
        }
        try {
            function.execute(destination);
        } catch (AbapException e) {
            System.out.println(e.toString());
            return e.toString();
        }

        JCoStructure exportStructure = function.getExportParameterList().getStructure("RFCSI_EXPORT");
        stringBuilder.append("System info for " + destination.getAttributes().getSystemID() + ":" + NL);
        for (int i = 0; i < exportStructure.getMetaData().getFieldCount(); i++) {
            stringBuilder.append(exportStructure.getMetaData().getName(i) + ":" + TAB + exportStructure.getString(i) + NL);
        }

        //JCo still supports the JCoFields, but direct access via getXX is more efficient as field iterator
        stringBuilder.append("\r\nThe same using field iterator: \r\nSystem info for " + destination.getAttributes().getSystemID() + ":" + NL);
        for (JCoField field : exportStructure) {
            stringBuilder.append(field.getName() + ":" + TAB + field.getString() + NL);
        }
        System.out.println(stringBuilder.toString());
        return stringBuilder.toString();
    }

    private static String messageHelper(String header, String message) {
        return "-----BEGIN " + header + "-----" + NL + message + NL + "------END " + header + "------" + NL;
    }

    public static String step4WorkWithTable(String functionName) throws JCoException {
        JCoDestination destination = sapGetDestination(DESTINATION_NAME2);
        functionName = "".equals(functionName) ? "BAPI_COMPANYCODE_GETLIST" : functionName;
        JCoFunction function = destination.getRepository().getFunction(functionName);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(messageHelper("function", function.toString()));
        if (function == null) {
            throw new RuntimeException(functionName + " not found in SAP.");
        }
        try {
            function.execute(destination);
        } catch (AbapException e) {
            System.out.println(e.toString());
            return e.toString();
        }
        stringBuilder.append(messageHelper("function.getFunctionTemplate()", function.getFunctionTemplate().toString()));
        stringBuilder.append(messageHelper("function.getExportParameterList()",function.getExportParameterList().toString()));
        JCoStructure returnStructure = function.getExportParameterList().getStructure("RETURN");
        stringBuilder.append(messageHelper("function.getExportParameterList().getStructure(\"RETURN\")", returnStructure.toString()));
        if (!(returnStructure.getString("TYPE").equals("") || returnStructure.getString("TYPE").equals("S"))) {
            throw new RuntimeException(returnStructure.getString("MESSAGE"));
        }
        stringBuilder.append(messageHelper("function.getTableParameterList()", function.getTableParameterList().toString()));
        stringBuilder.append(messageHelper("function.getTableParameterList().getTable(\"COMPANYCODE_LIST\")", function.getTableParameterList().getTable("COMPANYCODE_LIST").toString()));
        JCoTable codes = function.getTableParameterList().getTable("COMPANYCODE_LIST");
        for (int i = 0; i < codes.getNumRows(); i++) {
            codes.setRow(i);
            stringBuilder.append(codes.getString("COMP_CODE") + TAB + codes.getString("COMP_NAME") + NL);
        }
        codes.firstRow();
        for (int i = 0; i < codes.getNumRows(); i++, codes.nextRow()) {
            function = destination.getRepository().getFunction("BAPI_COMPANYCODE_GETDETAIL");
            if (function == null) {
                throw new RuntimeException("BAPI_COMPANYCODE_GETDETAIL not found in SAP.");
            }
            function.getImportParameterList().setValue("COMPANYCODEID", codes.getString("COMP_CODE"));
            function.getExportParameterList().setActive("COMPANYCODE_ADDRESS", false);

            try {
                function.execute(destination);
            } catch (AbapException e) {
                System.out.println(e.toString());
                return e.toString();
            }
            returnStructure = function.getExportParameterList().getStructure("RETURN");
            if (!(returnStructure.getString("TYPE").equals("") ||
                    returnStructure.getString("TYPE").equals("S") ||
                    returnStructure.getString("TYPE").equals("W"))) {
                throw new RuntimeException(returnStructure.getString("MESSAGE"));
            }

            JCoStructure detail = function.getExportParameterList().getStructure("COMPANYCODE_DETAIL");

            stringBuilder.append(detail.getString("COMP_CODE") + TAB +
                    detail.getString("COUNTRY") + TAB +
                    detail.getString("CITY") + NL);
        }
        System.out.println(stringBuilder.toString());
        return stringBuilder.toString();
    }

}

