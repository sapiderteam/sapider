package se.strivor.sapider.sap;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.JsonSerializer;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;

public class SapFunctionNameSerializer extends JsonSerializer<SapFunctionName> {

    private static final String baseUrl = "/sap/function/";

    @Value("${server.protocol}")
    private String protocol;

    @Value("${server.address}")
    private String address;

    @Value("${server.port}")
    private String port;

    public void serialize(SapFunctionName value, JsonGenerator generator, SerializerProvider provider)
            throws IOException, JsonProcessingException {

        if ("".equals(value.getAddress())) {
            value.setAddress(protocol + address + (port != null ? ":" + port : ""));
            String href = value.getLinks().get("self").get("href");
            href = value.getAddress() + baseUrl + href;
            value.getLinks().get("self").put("href", href);
        }
        generator.writeStartObject();
        generator.writeFieldName("function");
        generator.writeString(value.toString());
        generator.writeFieldName("description");
        generator.writeString(value.getDescription());
        generator.writeObjectField("links", value.getLinks());
        generator.writeEndObject();
    }
}
