package se.strivor.sapider.sap;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.HashMap;
import java.util.Map;

@JsonSerialize(using=SapFunctionNameSerializer.class)
public enum SapFunctionName {

    __LGC__SDF_SEARCH_MATERIAL(""),
    __LGC__SDF_GET_STOCK_INFO(""),
    __LGC__SDF_SEARCH_CUSTOMER(""),
    __LGC__SDF_LAS_CYL_BAL(""),
    __LGC__LAS_PRICE_SUM (""),
    __LGC__SDF_LAS_ICC_DATA(""),
    Y_NET_VALUE_KUNNR_MATNR(""),
    Y_ADR_DECLARATION(""),
    BAPI_CURRENCY_GETDECIMALS(""),
    BAPI_COMPANYCODE_GETLIST(""),
    BAPI_COMPANYCODE_GETDETAIL(""),
    RFC_SYSTEM_INFO(""),
    STFC_CONNECTION(""),
    __AGA__SDFG_PORTAL("Function group for Portal objects"),
    __AGA__SDF_ADD_CUSTOM_FAVORITES("Add customer to favorites"),
    __AGA__SDF_ADD_ORDER_ITEM_TEXT("add order item text"),
    __AGA__SDF_BAPI_CONVERT_TO_UTC("Search formaterials"),
    __AGA__SDF_BAPI_RETURN("Create return order"),
    __AGA__SDF_BAPI_RETURN_PDF_FILES("Return name and path of the PDF file"),
    __AGA__SDF_BAPI_SD_CHANGE("Sales order: Change Sales Order"),
    __AGA__SDF_CASH_BALANCE_LIST("Cash balance list"),
    __AGA__SDF_CHANGE_CUSTOMER("Change customer"),
    __AGA__SDF_CHANGE_ORDER("Change order, portal interface"),
    __AGA__SDF_CHANGE_PGI_DATE("Change PGI date"),
    __AGA__SDF_CHANGE_STOCK_TRANSFER("Get details for Replenishment Delivery"),
    __AGA__SDF_CHECK_CUSTOMER("Check customer"),
    __AGA__SDF_CHECK_FROM_TO_PLANT("Check plants (Stock transfer)"),
    __AGA__SDF_CHECK_GET_MATERIAL("Check manuel entered material and return description"),
    __AGA__SDF_CHECK_ICC("Check the manuel entered ICC number"),
    __AGA__SDF_CHECK_ONLY_RETURN("Get HHC order data (Used for portal)"),
    __AGA__SDF_CHECK_ORDER_NUMBER("Check manual entered order number"),
    __AGA__SDF_CHECK_TELEFON("Change telefonnumber"),
    __AGA__SDF_CHECK_UNPROC_DELIVERY("Create purchase order   > See transaction ZLPD"),
    __AGA__SDF_COMPLAIN_RUSH("Complain as rush order"),
    __AGA__SDF_COMPLAIN_STANDARD_OR("Complain, as standard order"),
    __AGA__SDF_CREATE_CUSTOMER("Create_customer"),
    __AGA__SDF_CREATE_DELIVERY("Create deliver"),
    __AGA__SDF_CREATE_PURCHASE_ORDER("Create purchase order   > See transaction ZLPD"),
    __AGA__SDF_CREATE_RUSH_ORDER("Create rush order"),
    __AGA__SDF_CREATE_SO_STOCK("Create standard order"),
    __AGA__SDF_CREATE_STANDARD_ORDER("Create standard order"),
    __AGA__SDF_DELETE_DELIVERY("Delete delivery"),
    __AGA__SDF_DEL_CUSTOM_FAVORITES("Delete customer from favorites"),
    __AGA__SDF_DISPLAY_PICKING_LIST("Print  picking list from Spool"),
    __AGA__SDF_DOCUMENT_FLOW("Show document flow for document"),
    __AGA__SDF_EVEN_EXCHANG_OR_LEASE("Get evn exchange or type of lease indicator"),
    __AGA__SDF_FORMAT_DATE("Format date into user date format"),
    __AGA__SDF_GET_AG_WE_LANGUAGE("Get language key for sold-to and shop-to party"),
    __AGA__SDF_GET_CHANGE_CUSTOMER("Find customer data to the portal for the customer to be changed"),
    __AGA__SDF_GET_CITY_NAME("Read city name"),
    __AGA__SDF_GET_CUSTOMER("Get HHC customer (Used for portal)"),
    __AGA__SDF_GET_CUSTOMER_LIST("Search for ship to  or favorit customers"),
    __AGA__SDF_GET_DATE_TIME("Get the date and time"),
    __AGA__SDF_GET_DEFAULT_MATERIAL("Get material for country code"),
    __AGA__SDF_GET_HHC_ORDERS("Get HHC delivery and returns (Used for portal)"),
    __AGA__SDF_GET_HHC_ORDERS_DETAIL("Get HHC order data (Used for portal)"),
    __AGA__SDF_GET_HHC_SHIP_DETAIL("Get HHC shipment detail (Used for portal)"),
    __AGA__SDF_GET_HHC_TRANSACTIONS("Get HHC transactions (Used for portal)"),
    __AGA__SDF_GET_INTERNAL_NOTE("Get internal note on customer"),
    __AGA__SDF_GET_LEASE_PERIOD("Get lease period in from to format"),
    __AGA__SDF_GET_MATERIAL("Get material for country code"),
    __AGA__SDF_GET_ORDER_TEXT("Get internal note on customer"),
    __AGA__SDF_GET_SHIP_TO_SOLD_TO("Gets Ship-to information and the corresponding sold-to"),
    __AGA__SDF_GET_SOLD_TO("Gets SOLD-TO from given SHIP-TO"),
    __AGA__SDF_GET_STOCK_INFO("Reads stock information to materials of specific plant"),
    __AGA__SDF_GET_STOCK_MATERIAL("Check manuel entered material and return description"),
    __AGA__SDF_GET_STOCK_TRANSFER("Get details for Replenishment Delivery"),
    __AGA__SDF_GET_SYSTEM_MESSAGES("Get DWI system messages"),
    __AGA__SDF_GET_USER_INFORMATION("Get user information"),
    __AGA__SDF_HHC_CHANGE_NAME("Change name on HHC transaction"),
    __AGA__SDF_HHC_CREATE_ORDER("Create an rush order (Used for portal)"),
    __AGA__SDF_HHC_ORDER_DELETE("Delete HHC order"),
    __AGA__SDF_INCOMING_STOCK_TRANSP("Create purchase order   > See transaction ZLPD"),
    __AGA__SDF_INDIVIDUAL_INFO("View individual delivery"),
    __AGA__SDF_LINE_OF_BUSINESS("Value table for Line of business in create customer"),
    __AGA__SDF_LIST_OF_DELIVERIES("List of deliveries"),
    __AGA__SDF_LIST_SALES_ORDERS_D("List sales orders (Used for portal)"),
    __AGA__SDF_LIST_SALES_ORDERS_H("Get HHC order data (Used for portal)"),
    __AGA__SDF_MANUAL_PGI_DISPLAY("Display  delivery"),
    __AGA__SDF_MANUAL_PGI_POST("Edit, save, and post delivery"),
    __AGA__SDF_MAT_FAVOURITES("Add / Delete materials to favorites"),
    __AGA__SDF_MESSAGE_TEXT_BUILD("Set up a message with parameter"),
    __AGA__SDF_OPEN_DELIVERIES("List all open deliveries"),
    __AGA__SDF_OPEN_DEL_STOCK_TRANSP("Create purchase order   > See transaction ZLPD"),
    __AGA__SDF_POST_USING_YSDEP_ZSM2("Post goods issue using transaction YSDEP_ZSM2"),
    __AGA__SDF_PRICE_QUOTATION("Price quotation"),
    __AGA__SDF_PRICE_QUOTATION_LIST("Price quotation"),
    __AGA__SDF_QUANTITY_ALLOWED("Get evn exchange or type of lease indicator"),
    __AGA__SDF_READ_CONFIG_TAB("Get the date and time"),
    __AGA__SDF_REJECT_ORDER("Change order, portal interface"),
    __AGA__SDF_REQ_DELIVERY_DATE("Requested delivery date"),
    __AGA__SDF_REQ_DEL_DATE("Requested delivery date"),
    __AGA__SDF_RETURN_FULL_CYL_RUSH("Return of full cylinder, rush order"),
    __AGA__SDF_RETURN_FULL_CYL_ST("Return of full cylinder, standard order"),
    __AGA__SDF_RET_COMP_ORDER_REASON("Get HHC delivery and returns (Used for portal)"),
    __AGA__SDF_RET_CYL_CREATE_ORDER("Create an rush order (Used for portal)"),
    __AGA__SDF_RET_CYL_VIEW_ICC_INFO("Get HHC delivery and returns (Used for portal)"),
    __AGA__SDF_SAVE_DELIVERY("Save delivery"),
    __AGA__SDF_SAVE_INSTRUCTION("View individual delivery"),
    __AGA__SDF_SEARCH_CUSTOMER("Search for customers"),
    __AGA__SDF_SEARCH_FREIGHT("Search for freight"),
    __AGA__SDF_SEARCH_MATERIAL("Search for materials"),
    __AGA__SDF_SEARCH_ORDER("Display Change order, portal interface"),
    __AGA__SDF_SERIALIZED("Get serialized indicator"),
    __AGA__SDF_TEXT_FROM_PORTAL("Format text from portal into SAP text format"),
    __AGA__SDF_TEXT_TO_PORTAL("Format texts into portal format"),
    __AGA__SDF_UPDATE_CDES_MAN_PGI("Update CDES tables when manuel PGI"),
    __AGA__SDF_UPDATE_SHIPMENT("Update shipment"),
    __AGA__SDF_UPDAT_UNPROC_DELIVERY("Create purchase order   > See transaction ZLPD"),
    __AGA__SDF_VIEW_ICC_INFO("Get ICC numbers for shipment and returns (Used for portal)"),
    __AGA__SDF_VIEW_IND_DELIVERY("View individual delivery"),
    __AGA__SDF_VL10_DELIVERY_CREATE("Create Replenishment Delivery"),
    YSDFGWWC_BUY_CERTI("Buy New Certificates"),
    YSDFGWWC_CYL_ACC_BAL("Cylinder Account Balance View"),
    YSDFGWWC_GET_CERTI_RENTGRP("Rent Certificate Of material rent Group"),
    YSDFGWWC_GET_MAT_RENT_GRP("Providing the search help for the Material Rent Group at Portal"),
    YSDFGWWC_RENEW_CERTI("Renew Certificates"),
    YSDFGWWC_RNT_CALC("Customer portal Rent Calculator"),
    Y_NDF_RV_MESSAGE_PRINT_SINGLE("Einzelausgabe von Drucknachrichten"),
    Y_NDF_RFC_ADD_MAT_TO_DATABASE("Adds the additional materials to the database (Internal Use only)"),
    Y_NDF_RFC_CHECK_ADDED_MATERIAL("Checks manuall added material items"),
    Y_NDF_RFC_CHECK_ADD_MAT_TXTYPE("Checks TXTYPE of added materials"),
    Y_NDF_RFC_CHECK_AND_POST("Checks the data and creates the sales documents in SAP"),
    Y_NDF_RFC_CHECK_HHC_READING("Checks HHC Reading"),
    Y_NDF_RFC_CHECK_HHC_TXTYPE("Checks TXTYPE of HHC Reading"),
    Y_NDF_RFC_CHECK_TRANSACT_TYPE("Checks the transaction type"),
    Y_NDF_RFC_DELETE_HHC_READING("NDF_RFC: Delete HHC Reading"),
    Y_NDF_RFC_GET_CUSTOMERS("Search for customers"),
    Y_NDF_RFC_GET_HHC_DATA("Get the readings from the Hand Held Computer (HHC)"),
    Y_NDF_RFC_GET_HHC_READING_CONT("NDF_RFC: Get the contents of a HHC Reading"),
    Y_NDF_RFC_GET_MATERIALS("Get the materials assigned to a plant"),
    Y_NDF_RFC_GET_ORDERS("Get orders for plant"),
    Y_NDF_RFC_GET_ORG_DATA("Get the organizational data for a plant"),
    Y_NDF_RFC_KUNNR_CHECK("Check if a customer number exists in a plant"),
    Y_NDF_RFC_KUNNR_CHECK_GET_TEXT("Checks if a customers exists in a plant and gets the internal note (Te"),
    Y_NDF_RFC_MESSAGE_TEXT_BUILD("Set up a message with parameter");

    private final String description;
    private Map<String, Map<String, String>> links;
    private String address = "";

    SapFunctionName(String description) {
        this.description = description;
        this.links = new HashMap<>();
        Map<String, String> self = new HashMap<>();
        self.put("href", this.toString());
        this.links.put("self", self);
    }

    public String getDescription() {
        return description;
    }

    Map<String, Map<String, String>> getLinks() {
        return links;
    }

    public void setLinks(Map<String, Map<String, String>> links) {
        this.links = links;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        String name = name();
        name = name.replace("__", "/");
        return name;
    }
}
