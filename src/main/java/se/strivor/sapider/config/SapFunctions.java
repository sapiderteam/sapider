package se.strivor.sapider.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import se.strivor.sapider.data.SapFunction;

import java.util.List;

@Component
@PropertySource("classpath:sapFunctions.yml")
@ConfigurationProperties
public @Data class SapFunctions {

    private List<SapFunction> sapFunctionList;
}
