package se.strivor.sapider.sapapi;

import com.sap.conn.jco.JCoException;
import com.sap.conn.jco.JCoFunction;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiImplicitParam;
//import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import se.strivor.sapider.data.SapMetaData;
import se.strivor.sapider.sap.SapFunctionName;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static se.strivor.sapider.sap.TestClient.*;

@RestController
@RequestMapping("/sap")
//@Api(tags = {"sap"})
public class SAPConnect {

    @RequestMapping(value = "/function", method = GET)
//    @ApiOperation(value = "Listing of SAP Functions", notes = "List all SAP functions")
    public List<SapFunctionName> sapListFunctions() {
        List<SapFunctionName> functions = new ArrayList<>();
        for (SapFunctionName func : SapFunctionName.values()) {
            functions.add(func);
        }
        return functions;
    }

    @RequestMapping(value = "/function/{prefix}/{functionName}", method = GET)
//    @ApiOperation(value = "List SAP Function", notes = "Detailed listing of a SAP function")
    public Map<String, List<SapMetaData>> sapListPrefixFunction(@PathVariable String prefix, @PathVariable String functionName) {
        return sapListFunction("/" + prefix + "/" + functionName);
    }

//    @RequestMapping(value = "/function/AGA/{functionName}", method = GET)
//    @ApiOperation(value = "List SAP Function", notes = "Detailed listing of a SAP function")
//    public Map<String, List<SapMetaData>> sapListAgaFunction(@PathVariable String functionName) {
//        return sapListFunction("/AGA/" + functionName);
//    }

    @RequestMapping(value = "/function/{functionName}", method = GET)
//    @ApiOperation(value = "List SAP Function", notes = "Detailed listing of a SAP function")
    public Map<String, List<SapMetaData>> sapListFunction(@PathVariable String functionName) {
        Map<String, List<SapMetaData>> ret = null;
        try {
            ret = sapGetListMetadata(functionName);
        }
        catch (JCoException e) {
            Throwable t = e.getCause();
            System.out.println(t.getMessage());
        }

        return ret;
    }

    @RequestMapping(value = "/function/{prefix}/{functionName}/desc", method = GET)
//    @ApiOperation(value = "Describe SAP Function", notes = "Detailed description of a SAP function")
    public String sapDescribePrefixFunction(@PathVariable String prefix, @PathVariable String functionName) {
        return sapDescribeFunction("/" + prefix + "/" + functionName);
    }

    @RequestMapping(value = "/function/{functionName}/desc", method = GET)
//    @ApiOperation(value = "Describe SAP Function", notes = "Detailed description of a SAP function")
    public String sapDescribeFunction(@PathVariable String functionName) {
        JCoFunction func = null;
        try {
            func = sapGetFunction(functionName);
        }
        catch (JCoException e) {
            Throwable t = e.getCause();
            System.out.println(t.getMessage());
        }
        String s = sapGetFunctionDetails(func);
        return html(s);
    }

    @RequestMapping(value = {"/test", "/test/{id}"}, method = GET)
//    @ApiOperation(value = "Test of SAP Basic Functions", notes = "Fast way to run basic SAP functions")
//    @ApiImplicitParam(name = "id", dataType = "int", paramType = "path")
    public String sapIndex(@PathVariable(required = false) Integer id, @RequestParam(required = false, defaultValue = "") String functionName) {
        String result = "";
        id = id == null ? 0 : id;
        try {
            switch (id) {
                case 1:
                    result = step1Connect();
                    break;
                case 2:
                    result = step2ConnectUsingPool();
                    break;
                case 3:
                    result = step3SimpleCall();
                    break;
                case 4:
                    result = step3WorkWithStructure();
                    break;
                case 5:
                    result = step4WorkWithTable(functionName);
                    break;
                default:
                    result = "Tjääna!";

            }
        }
        catch (JCoException e) {
            Throwable t = e.getCause();
            System.out.println(t.getMessage());
            result = e.toString() + "\n\nmessage: " + e.getMessage();
            System.out.println(result);
        }
        return html(result);
    }

    // Helper
    private static String html(String str) {
        return "<pre>" + str + "</pre>";
    }
}
