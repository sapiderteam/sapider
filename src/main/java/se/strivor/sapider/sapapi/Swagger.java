package se.strivor.sapider.sapapi;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
//import springfox.documentation.annotations.ApiIgnore;

@Controller
//@ApiIgnore
public class Swagger {

    @RequestMapping("/swaggerapi")
    public String index() {
        System.out.println("redirect:swagger-ui.html");
        return "/swagger-ui.html";
    }

}
